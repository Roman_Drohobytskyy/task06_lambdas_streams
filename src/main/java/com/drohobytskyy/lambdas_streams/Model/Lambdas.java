package com.drohobytskyy.lambdas_streams.Model;

import com.drohobytskyy.lambdas_streams.View.ConsoleView;
import com.drohobytskyy.lambdas_streams.View.MenuItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Lambdas {
    private BufferedReader input;
    private Map<String, MenuItem> menu;
    public ConsoleView view;

    public Lambdas() {
        input = new BufferedReader(new InputStreamReader(System.in));
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem("   1      - find max", this::returnLambdaMax));
        menu.put("2", new MenuItem("   2      - find average", this::returnLambdaAverage));
        menu.put(view.BACK, new MenuItem(view.BACK + "      - go back", () -> view.executeMenu(view.menu)));
        menu.put(view.EXIT, new MenuItem(view.EXIT + "      - exit from app", () -> view.exitFromMenu()));

    }

    public void executeMenu() throws IOException {
        view.executeMenu(menu);
    }

    public void returnLambdaMax() throws IOException {
        int value1 = inputInteger("value 1");
        int value2 = inputInteger("value 2");
        int value3 = inputInteger("value 3");
        ThreeIntValues lambdaMax = (a, b, c) -> Math.max(a, Math.max(b, c));
        view.logger.info("Max value: " + lambdaMax.find(value1, value2, value3));
    }

    public void returnLambdaAverage() throws IOException {
        int value1 = inputInteger("value 1");
        int value2 = inputInteger("value 2");
        int value3 = inputInteger("value 3");
        ThreeIntValues lambdaMax = (a, b, c) -> (a + b + c) / 3;
        view.logger.info("Average value: " + lambdaMax.find(value1, value2, value3));
    }

    private int inputInteger(final String variableDescription) throws IOException {
        String stringFromBufferedReader;
        boolean isIntegerFounded = false;
        do {
            view.logger.info("Please, enter value " + variableDescription
                    + " : ");
            stringFromBufferedReader = input.readLine();
            if (!isInteger(stringFromBufferedReader)) {
                view.logger.error("Incorrect value! Please, try again.");
            } else {
                isIntegerFounded = !isIntegerFounded;
            }
        } while (!isIntegerFounded);
        return Integer.parseInt(stringFromBufferedReader);
    }

    private static boolean isInteger(final String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }
}
