package com.drohobytskyy.lambdas_streams.Model;

import com.drohobytskyy.lambdas_streams.View.ConsoleView;
import com.drohobytskyy.lambdas_streams.View.MenuItem;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

public class RandomIntegers {
    private int[] array;
    private Map<String, MenuItem> menu;
    public ConsoleView view;

    public RandomIntegers() {
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem("   1      - print statistics", this::printStatistics));
        menu.put("2", new MenuItem("   2      - print count of values > avg",
                this::printCountOfValuesBiggerThanAverage));
        menu.put(view.BACK, new MenuItem(view.BACK + "      - go back", () -> view.executeMenu(view.menu)));
        menu.put(view.EXIT, new MenuItem(view.EXIT + "      - exit from app", () -> view.exitFromMenu()));
        generate();
    }

    public void executeMenu() throws IOException {
        view.executeMenu(menu);
    }

    private void printStatistics() {
        view.logger.info(Arrays.toString(array));
        findMin();
        findMax();
        findSum();
        view.logger.info(findAvg());
    }

    private void printCountOfValuesBiggerThanAverage() {
        view.logger.info(Arrays.toString(array));
        view.logger.info("Average = " + findAvg());
        findNumbersBiggerThanAvg();
    }

    private void generate() {
        final int LENGTH_OF_ARRAY = 10;
        Random random = new Random();
        array = IntStream.iterate(
                random.nextInt(10), n -> random.nextInt(10)).limit(LENGTH_OF_ARRAY).toArray();
    }

    public void findMin() {
        view.logger.info("Min = " + Arrays.stream(array).min().getAsInt());
    }

    public void findMax() {
        view.logger.info("Max = " + Arrays.stream(array).max().getAsInt());
    }

    public double findAvg() {
        double avg = Arrays.stream(array).average().getAsDouble();
        return avg;
    }

    public void findSum() {
        view.logger.info("Sum = " + Arrays.stream(array).sum());
        view.logger.info("Sum = " + Arrays.stream(array).reduce((a, b) -> a + b).getAsInt());
    }

    public void findNumbersBiggerThanAvg() {
        double avg = findAvg();
        view.logger.info("Numbers that are bigger than average: ");
        Arrays.stream(array).filter(a -> a > avg).forEach(a -> view.logger.info(a + ", "));
        view.logger.info("Count of numbers that are bigger than average: "
                + Arrays.stream(array).filter(a -> a > findAvg()).count());
    }
}
