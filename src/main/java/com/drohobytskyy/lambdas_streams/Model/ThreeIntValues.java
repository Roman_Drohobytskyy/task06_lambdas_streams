package com.drohobytskyy.lambdas_streams.Model;
@FunctionalInterface
public interface ThreeIntValues {
    int find(int a, int b, int c);
}
