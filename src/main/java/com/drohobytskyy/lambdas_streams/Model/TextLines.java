package com.drohobytskyy.lambdas_streams.Model;

import com.drohobytskyy.lambdas_streams.View.ConsoleView;
import com.drohobytskyy.lambdas_streams.View.MenuItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class TextLines {
    private BufferedReader input;
    public ConsoleView view;
    private Map<String, MenuItem> menu;
    List<String> listOfLines;

    public TextLines() {
        input = new BufferedReader(new InputStreamReader(System.in));
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem("   1      - input lines", this::inputText));
        menu.put("2", new MenuItem("   1      - print statistics", this::printStatistics));
        menu.put("3", new MenuItem("   2      - Occurrence number of each word in the text",
                this::printOccurrenceNumberOfEachWord));
        menu.put(view.BACK, new MenuItem(view.BACK + "      - go back", () -> view.executeMenu(view.menu)));
        menu.put(view.EXIT, new MenuItem(view.EXIT + "      - exit from app", () -> view.exitFromMenu()));
    }

    public void executeMenu() throws IOException {
        view.executeMenu(menu);
    }

    private void inputText() throws IOException {
        listOfLines = new ArrayList();
        String inputText = "";
        do {
            System.out.print("Please, enter next line: ");
            inputText = input.readLine();
            if (!input.equals("")) {
                listOfLines.add(inputText);
            }
        } while (!inputText.equals(""));
        System.out.println(listOfLines);
    }

    private void printStatistics() throws IOException {
        if (listOfLines == null) {
            inputText();
        }
        view.logger.info("Number of elements in the stream: "
                + listOfLines.stream()
                .count());
        view.logger.info("List of unique elements in the stream: "
                + listOfLines.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList()));
        view.logger.info("Number of unique elements in the stream: "
                + listOfLines.stream()
                .distinct()
                .count());
    }

    private void printOccurrenceNumberOfEachWord() throws IOException {
        if (listOfLines == null) {
            inputText();
        }
        view.logger.info("Occurrence number of each word in the text: " + listOfLines.stream()
                .collect(Collectors.groupingBy(f -> f, Collectors.counting())));
    }
}
