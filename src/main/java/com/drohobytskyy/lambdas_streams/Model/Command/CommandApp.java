package com.drohobytskyy.lambdas_streams.Model.Command;

import com.drohobytskyy.lambdas_streams.View.ConsoleView;
import com.drohobytskyy.lambdas_streams.View.MenuItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandApp {
    private Comp comp;
    private User user;
    private BufferedReader input;
    public ConsoleView view;
    public Map<String, MenuItem> menu;

    public CommandApp() {
        comp = new Comp();
        user = new User(new StartCommand(comp), new StopCommand(comp),
                new ResetCommand(comp, view), new ConnectCommand(comp, view));
        input = new BufferedReader(new InputStreamReader(System.in));
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem("   1      - start computer", this::startComputer));
        menu.put("2", new MenuItem("   2      - stop computer", this::stopComputer));
        menu.put("3", new MenuItem("   3      - reset computer", this::resetComputer));
        menu.put("4", new MenuItem("   4      - connect computer", this::connectComputer));
        menu.put(view.BACK, new MenuItem(view.BACK + "      - go back", () -> view.executeMenu(view.menu)));
        menu.put(view.EXIT, new MenuItem(view.EXIT + "      - exit from app", () -> view.exitFromMenu()));
    }

    public void executeMenu() throws IOException {
        view.executeMenu(menu);
    }

    private void startComputer() throws IOException {
        user.startComputer(inputTextFromConsole());
    }

    private void stopComputer() throws IOException {
        user.stopComputer(inputTextFromConsole());
    }

    private void resetComputer() throws IOException {
        user.resetComputer(inputTextFromConsole());
    }

    private void connectComputer() throws IOException {
        user.connectComputer(inputTextFromConsole());
    }

    private String inputTextFromConsole() throws IOException {
        view.logger.warn("Please enter a text as input parameter for the method: ");
        String inputText = input.readLine();
        return inputText;
    }
}
