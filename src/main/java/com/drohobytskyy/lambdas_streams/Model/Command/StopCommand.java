package com.drohobytskyy.lambdas_streams.Model.Command;

class StopCommand implements Command {
    Comp comp;

    public StopCommand(Comp comp) {
        this.comp = comp;
    }

    @Override
    public void execute(String message) {
        Command lambda = (s) -> System.out.println(s);
        lambda.execute("Stop computer (Lambda)" + message);
    }
}