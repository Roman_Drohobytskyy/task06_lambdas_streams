package com.drohobytskyy.lambdas_streams.Model.Command;

public class User {
    Command start;
    Command stop;
    Command reset;
    Command connect;

    public User(Command start, Command stop, Command reset, Command connect) {
        this.start = start;
        this.stop = stop;
        this.reset = reset;
        this.connect = connect;
    }

    void startComputer(String message) {
        start.execute(message);
    }

    void stopComputer(String message) {
        stop.execute(message);
    }

    void resetComputer(String message) {
        reset.execute(message);
    }

    void connectComputer(String message) {
        connect.execute(message);
    }
}
