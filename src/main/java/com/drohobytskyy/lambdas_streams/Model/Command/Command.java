package com.drohobytskyy.lambdas_streams.Model.Command;

public interface Command {
    void execute(String message);
}
