package com.drohobytskyy.lambdas_streams.Model.Command;

public class Comp {
    void start(String message) {
        System.out.println("Start the computer " + message);
    }

    void stop(String message) {
        System.out.println("Stop the computer " + message);
    }

    void reset(String message) {
        System.out.println("Reset the computer " + message);
    }
}
