package com.drohobytskyy.lambdas_streams.Model.Command;

import com.drohobytskyy.lambdas_streams.View.ConsoleView;

class ResetCommand implements Command {
    Comp comp;
    ConsoleView view;

    public ResetCommand(Comp comp, ConsoleView view) {
        this.comp = comp;
        this.view = view;
    }

    @Override
    public void execute(String message) {
        Command anonymousCommand = new Command() {
            @Override
            public void execute(String message) {
                System.out.println("Reset computer (Anonymous)" + message);
            }
        };
        anonymousCommand.execute(message);
    }
}

