package com.drohobytskyy.lambdas_streams.Model.Command;

import com.drohobytskyy.lambdas_streams.View.ConsoleView;

import java.util.function.Consumer;

public class ConnectCommand implements Command {
    Comp comp;
    ConsoleView view;

    public ConnectCommand(Comp comp, ConsoleView view) {
        this.comp = comp;
        this.view = view;
    }

    @Override
    public void execute(String message) {
        Consumer<String> printer = this::connect;
        printer.accept(message);
    }

    private void connect(String message) {
        Consumer<String> printer = System.out::println;
        printer.accept(message + "(lambda method link)");
    }
}
