package com.drohobytskyy.lambdas_streams.Model.Command;

class StartCommand implements Command {
    Comp comp;

    public StartCommand(Comp comp) {
        this.comp = comp;
    }

    @Override
    public void execute(String message) {
        comp.start(message);
    }
}