package com.drohobytskyy.lambdas_streams.controller;

import com.drohobytskyy.lambdas_streams.Model.Command.CommandApp;
import com.drohobytskyy.lambdas_streams.Model.Command.Comp;
import com.drohobytskyy.lambdas_streams.Model.Lambdas;
import com.drohobytskyy.lambdas_streams.Model.RandomIntegers;
import com.drohobytskyy.lambdas_streams.Model.TextLines;
import com.drohobytskyy.lambdas_streams.View.ConsoleView;
import java.io.IOException;

public class Controller {
    CommandApp app;
    ConsoleView consoleView;
    Lambdas lambdas;
    RandomIntegers randomIntegers;
    TextLines textLines;
    Comp comp = new Comp();

    public Controller(ConsoleView consoleView, Lambdas lambdas, CommandApp app,
                      RandomIntegers randomIntegers, TextLines textLines) {
        this.app = app;
        this.consoleView = consoleView;
        app.view = consoleView;
        this.lambdas = lambdas;
        lambdas.view = consoleView;
        this.randomIntegers = randomIntegers;
        randomIntegers.view = consoleView;
        this.textLines = textLines;
        textLines.view = consoleView;
        consoleView.controller = this;
        consoleView.createMenu();
        try {
            consoleView.executeMenu(consoleView.menu);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void executeLambdas() throws IOException {
        lambdas.executeMenu();
    }

    public void executeCommandApp() throws IOException {
        app.executeMenu();
    }

    public void executeTextLines() throws IOException {
        textLines.executeMenu();
    }

    public void executeRandomIntegers() throws IOException {
        randomIntegers.executeMenu();
    }
}
