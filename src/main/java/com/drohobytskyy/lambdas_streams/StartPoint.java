package com.drohobytskyy.lambdas_streams;

import com.drohobytskyy.lambdas_streams.Model.Command.CommandApp;
import com.drohobytskyy.lambdas_streams.Model.Lambdas;
import com.drohobytskyy.lambdas_streams.Model.RandomIntegers;
import com.drohobytskyy.lambdas_streams.Model.TextLines;
import com.drohobytskyy.lambdas_streams.View.ConsoleView;
import com.drohobytskyy.lambdas_streams.controller.Controller;

import java.io.IOException;

public class StartPoint {

    public static void main(String[] args) {
        Controller controller = new Controller(new ConsoleView(), new Lambdas(), new CommandApp(),
                new RandomIntegers(), new TextLines());

    }
}
