package com.drohobytskyy.lambdas_streams.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

import com.drohobytskyy.lambdas_streams.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView {
    private BufferedReader input;
    public Controller controller;
    public final static String BACK = "back";
    public final static String EXIT = "exit";
    public Map<String, MenuItem> menu;
    public Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public void createMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem("   1      - Functional interface (3 int input, 1 output)",
                controller::executeLambdas));
        menu.put("2", new MenuItem("   2      - Command application",
                controller::executeCommandApp));
        menu.put("3", new MenuItem("   3      - Random integers",
                controller::executeRandomIntegers));
        menu.put("4", new MenuItem("   4      - Text lines",
                controller::executeTextLines));
        menu.put(EXIT, new MenuItem(EXIT + "      - exit from app", this::exitFromMenu));
    }

    public void executeMenu(Map<String, MenuItem> menu) throws IOException {
        String keyMenu;
        do {
            logger.warn("Please, select menu point.");
            outputMenu(menu);
            keyMenu = input.readLine().toLowerCase();
            try {
                menu.get(keyMenu).getLink().print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (true);
    }

    private void outputMenu(Map<String, MenuItem> menu) {
        for (MenuItem pair : menu.values()) {
            logger.info(pair.getDescription());
        }
    }

    public void exitFromMenu() {
        logger.warn("Have an amazing day!");
        System.exit(0);
    }

}
